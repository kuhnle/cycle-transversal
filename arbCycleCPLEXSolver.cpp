#ifndef KLP_SOLVER_CPP
#define KLP_SOLVER_CPP

#include <ilcplex/ilocplex.h>
#include <unordered_set>
#include "ArbCycleGraph.cpp"

using namespace arbCycleGraph;

ILOSTLBEGIN


/*
 * Solves LP 1.
 * Output value for each edge e \in E is in v_sol
 * G must have a valid triangle-listing
 * Requires eid of each edge to be valid
 */
bool solve_lp ( Graph& G,
		vector< double >& v_sol,
		IloEnv& env,
		IloCplex& cplex,
		//		IloModel& model,
		unsigned nThreads = 18, bool bVerb = false ) {

   G.logg(INFO, "Beginning LP...");
   
   //Triangles should already be generated
   //   IloEnv env;
   IloModel model(env);
   IloNumVarArray var(env);
   IloRangeArray con(env);

   G.logg(DEBUG, "Adding variables to LP...");
   //For each edge, we need a variable
   // 0 \le x_i \le 1
   for (pedge e = G.E.begin();
	e != G.E.end();
	++e ) {
      //    var.add( IloNumVar(env, 0.0, 1.0) );
      var.add( IloNumVar(env) ); //shouldn't need constraints, >= 0 by default
   }

   // logg(DEBUG, "Building objective of LP...");
   // //Add the objective function, which is to
   // //min. the number of edges
   // IloExpr edge_obj( env, 0 );
   // edge_obj.setName( "Edge_objective" );
   // for (unsigned i = 0; i < G.E.size(); ++i) {
   //   edge_obj += var[i];
   // }

   // model.add( IloMinimize( env, edge_obj ) );

   // logg(DEBUG, "Adding triangle constraints to LP...");
   // //Depends on valid edge ids
   // IloRangeArray c(env);
   // for ( pangle t = G.T.begin();
   // 	t != G.T.end();
   // 	++t ) {
   //   c.add( var[ t->e1->eid ] + var[ t->e2->eid ] + var[ t->e3->eid ] >= 1.0 ); //each triangle must be broken
   // }

   // model.add(c);
   G.logg(DEBUG, "Adding constraints...");
  
   IloRangeArray c(env);
   IloObjective obj = IloMinimize( env );
   for (pcycle t = G.C.begin();
	t != G.C.end();
	++t ) {
      if (G.cycle_valid( t ))
	 c.add(IloRange( env, 1.0, IloInfinity ));
   }

   G.logg(DEBUG, "Setting objective coefficients...");
   unsigned eid = 0;
   for (pedge e = G.E.begin();
	e != G.E.end();
	++e ) {
      obj.setLinearCoef( var[ eid ], 1.0 );
      ++eid;
   }

   G.logg(DEBUG, "Setting constraint coefficients...");
   unsigned tid = 0;
   for (pcycle t = G.C.begin();
	t != G.C.end();
	++t ) {
      if (G.cycle_valid( t )) {
	 for (size_t j = 0; j < t->cyEdges.size(); ++j) {
	    c[tid].setLinearCoef(var[ t->cyEdges[j]->eid ], 1.0);
	 }
	 ++tid;
      }
   }

   model.add(obj);
   model.add(c);

   G.logg(DEBUG, "Building cplex from model...");
   //   IloCplex cplex(model);
   cplex.extract(model);
   cplex.setParam(IloCplex::IntParam::Threads, nThreads);
   cplex.setParam(IloCplex::ClockType, 2); //Tells cplex to use wall-clock time
   //double hour = 60.0 * 60;
   //cplex.setParam(IloCplex::TiLim, max_hours * hour );
   if (!bVerb)
      cplex.setOut(env.getNullStream());
   
   G.logg(DEBUG, "Solving LP...");

   cplex.setParam( IloCplex::Param::Preprocessing::Presolve, true );
   cplex.solve();

   eid = 0;

   v_sol.clear();
   v_sol.reserve( G.E.size() );
   for (pedge e = G.E.begin();
        e != G.E.end();
        ++e ) {
      v_sol.push_back( cplex.getValue( var[eid] ) );
      ++eid;
   }

   G.logg(INFO, "Solution finished, objective value: " + to_string(cplex.getObjValue()));
   if (cplex.getCplexStatus() == IloCplex::Optimal)
      return true;
   else
      return false;

   //   cplex.clearModel();
   cplex.end();
   obj.end();
   c.end();
   var.end();
   con.end();
   model.end();
   env.end();
}

/*
 * Solves IP 1 optimally.
 * Output value for each edge e \in E is in v_sol
 * G must have a valid triangle-listing
 * Requires eid of each edge to be valid
 */
unsigned solve_ip ( Graph& G,
		    vector< double >& v_sol,
		    unsigned nThreads = 18, bool bVerb = false ) {


   G.logg(INFO, "Beginning IP with maximum of " + to_string( nThreads ) + " threads...");
   
   //Triangles should already be generated
   IloEnv env;
   IloModel model(env);
   IloNumVarArray var(env);//, G.E.size(), 0, 1, ILOBOOL );
   IloRangeArray con(env);

   G.logg(DEBUG, "Adding variables to IP...");
   //   For each edge, we need a variable
   //   x_i \in {0,1}
   for (pedge e = G.E.begin();
   	e != G.E.end();
   	++e ) {
      //         var.add( IloNumVar(env, 0.0, 1.0) );
      var.add( IloNumVar(env,0.0,1.0, ILOINT )); 
   }

   // G.logg(DEBUG, "Building objective of LP...");
   // //Add the objective function, which is to
   // //min. the number of edges
   // IloExpr edge_obj( env, 0 );
   // edge_obj.setName( "Edge_objective" );
   // for (unsigned i = 0; i < G.E.size(); ++i) {
   //    edge_obj += var[i];
   // }

   // model.add( IloMinimize( env, edge_obj ) );

   // G.logg(DEBUG, "Adding triangle constraints to LP...");
   // //Depends on valid edge ids
   // IloRangeArray c(env);
   // for ( pangle t = G.T.begin();
   // 	 t != G.T.end();
   // 	 ++t ) {
   //    c.add( var[ t->e1->eid ] + var[ t->e2->eid ] + var[ t->e3->eid ] >= 1.0 ); //each triangle must be broken
   // }
   // model.add(c);
   G.logg(DEBUG, "Adding constraints...");
  
   IloRangeArray c(env);
   IloObjective obj = IloMinimize( env );
   for (pcycle t = G.C.begin();
    	t != G.C.end();
	++t ) {
      if (G.cycle_valid( t ))
	 c.add(IloRange( env, 1.0, IloInfinity ));
   }

   G.logg(DEBUG, "Setting objective coefficients...");
   unsigned eid = 0;
   for (pedge e = G.E.begin();
	 	e != G.E.end();
	 	++e ) {
      obj.setLinearCoef( var[ eid ], 1.0 );
      ++eid;
   }

   G.logg(DEBUG, "Setting constraint coefficients...");
   unsigned tid = 0;
   for (pcycle t = G.C.begin();
    	t != G.C.end();
	++t ) {
      if (G.cycle_valid( t )) {
	 for (size_t j = 0 ; j < t->cyEdges.size(); ++j) {
	    c[tid].setLinearCoef(var[ t->cyEdges[j]->eid ], 1.0);
	 }
	 
	 ++tid;
      }
   }

   model.add(obj);
   model.add(c);

   G.logg(DEBUG, "Building cplex from model...");
   IloCplex cplex(model);
   cplex.setParam(IloCplex::IntParam::Threads, nThreads);
   cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
   //   cplex.setParam(IloCplex::IntParam::Threads, 1);
   //Tells cplex to use wall-clock time
   cplex.setParam(IloCplex::ClockType, 2); 
   //double hour = 60.0 * 60;
   //cplex.setParam(IloCplex::TiLim, max_hours * hour );
   if (!bVerb)
      cplex.setOut(env.getNullStream());
   //   cplex.setParam(IloCplex::EpAGap, 1.0 / (2 * G.E.size()));
   G.logg(DEBUG, "Solving IP...");
   cplex.solve();

   eid = 0;

   v_sol.clear();
   v_sol.reserve( G.E.size() );
   for (pedge e = G.E.begin();
        e != G.E.end();
        ++e ) {
      v_sol.push_back( cplex.getValue( var[eid] ) );
      if ( cplex.getValue(var[eid]) == 1 )
	 e->in_S = true;
      ++eid;

   }

   G.logg(INFO, "Solution finished, objective value: " + to_string(cplex.getObjValue()));
   if (cplex.getCplexStatus() == IloCplex::Optimal)
      return static_cast<unsigned> ( cplex.getObjValue() );
   else
      return 0;
}

// /*
//  * Solves IP 1 optimally.
//  * Output value for each edge e \in E is in v_sol
//  * G must have a valid triangle-listing
//  * Requires eid of each edge to be valid
//  */
// unsigned solve_ip2 ( Graph& G,
// 		     vector< double >& v_sol,
// 		     unsigned nThreads = 18) {
//    G.logg(INFO, "Beginning IP with maximum of " + to_string( nThreads ) + " threads...");
   
//    //Triangles should already be generated
//    IloEnv env;
//    IloModel model(env);
//    IloNumVarArray var(env); // G.E.size(), 0, 1, ILOBOOL );
//    IloRangeArray con(env);

//    G.logg(DEBUG, "Adding variables to IP...");
//    //   For each edge, we need a variable
//    //   x_i \in {0,1}
//      for (pedge e = G.E.begin();
//    	e != G.E.end();
//    	++e ) {
// 	//         var.add( IloNumVar(env, 0.0, 1.0) );
// 	var.add( IloNumVar(env,0.0,1.0, ILOINT )); //shouldn't need constraints, >= 0 by default
//      }

//      G.logg(DEBUG, "Building objective of LP...");
//    //Add the objective function, which is to
//    //min. the number of edges
//    IloExpr edge_obj( env, 0 );
//    edge_obj.setName( "Edge_objective" );
//    for (unsigned i = 0; i < G.E.size(); ++i) {
//      edge_obj += var[i];
//    }

//    model.add( IloMinimize( env, edge_obj ) );

//    G.logg(DEBUG, "Adding triangle constraints to LP...");
//    //Depends on valid edge ids
//    IloRangeArray c(env);
//    for ( pangle t = G.T.begin();
//    	t != G.T.end();
//    	++t ) {
//      c.add( var[ t->e1->eid ] + var[ t->e2->eid ] + var[ t->e3->eid ] >= 1.0 ); //each triangle must be broken
//    }

//    model.add(c);
//    // G.logg(DEBUG, "Adding constraints...");
  
//    // IloRangeArray c(env);
//    // IloObjective obj = IloMinimize( env );
//    // for (pangle t = G.T.begin();
//    // 	t != G.T.end();
//    // 	++t ) {
//    //    if (triangle_valid( t ))
//    // 	 c.add(IloRange( env, 1.0, IloInfinity ));
//    // }

//    // G.logg(DEBUG, "Setting objective coefficients...");
//    // unsigned eid = 0;
//    // for (pedge e = G.E.begin();
//    // 	e != G.E.end();
//    // 	++e ) {
//    //    obj.setLinearCoef( var[ eid ], 1.0 );
//    //    ++eid;
//    // }

//    // G.logg(DEBUG, "Setting constraint coefficients...");
//    // unsigned tid = 0;
//    // for (pangle t = G.T.begin();
//    // 	t != G.T.end();
//    // 	++t ) {
//    //    if (triangle_valid( t )) {
//    // 	 c[tid].setLinearCoef(var[ t->e1->eid ], 1.0);
//    // 	 c[tid].setLinearCoef(var[ t->e2->eid ], 1.0);
//    // 	 c[tid].setLinearCoef(var[ t->e3->eid ], 1.0);
//    // 	 ++tid;
//    //    }
//    // }

//    //   model.add(obj);
//    //   model.add(c);

//    G.logg(DEBUG, "Building cplex from model...");
//    IloCplex cplex(model);
//    cplex.setParam(IloCplex::IntParam::Threads, nThreads);
//    cplex.setParam(IloCplex::IntParam::Threads, 1);
//    cplex.setParam(IloCplex::ClockType, 1); //Tells cplex to use cpu time
//    double hour = 60.0 * 60;
//    cplex.setParam(IloCplex::TiLim, 10 * hour );
//    cplex.setOut(env.getNullStream());
//    //   cplex.setParam(IloCplex::EpAGap, 1.0 / (2 * G.E.size()));
//    G.logg(DEBUG, "Solving IP...");
//    cplex.solve();

//    unsigned   eid = 0;

//    v_sol.clear();
//    v_sol.reserve( G.E.size() );
//    for (pedge e = G.E.begin();
//         e != G.E.end();
//         ++e ) {
//       v_sol.push_back( cplex.getValue( var[eid] ) );
//       if ( cplex.getValue(var[eid]) == 1 )
// 	 e->in_S = true;
//       ++eid;

//    }

//    G.logg(INFO, "Solution finished, objective value: " + to_string(cplex.getObjValue()));
//    if (cplex.getCplexStatus() == IloCplex::Optimal)
//       return static_cast<unsigned> ( cplex.getObjValue() );
//    else
//       return 0;
// }


/*
 * Kortsarz
 * (k-1)-approximation
 *
 */
unsigned kortsarz( Graph& G, size_t k, unsigned nThreads = 18, bool bVerb = false ) {
   G.logg(INFO, "Starting Kortsarz et al....");
   if (k % 2 == 0) {
      G.logg << ERROR << "KORTSARZ can only be used with odd cycles.";
      return false;
   }

   G.list_cycles( k, true );
   
   vector< double > lpsol;

   bool b_solve;
   unsigned eid;
   unsigned sizeS = 0;
   IloEnv env;
   IloCplex cplex( env );
   do {
      bool status = solve_lp( G, lpsol, env, cplex, nThreads, bVerb );
      if (status == false)
	 return 0;
      
      b_solve = false;
      eid = 0;
      G.logg(DEBUG, "Size of E: " + to_string( G.E.size() ));
      for (auto e = G.E.begin();
	   e != G.E.end();
	   ++e) {
	 if (lpsol[eid] > (1.0 / (k - 1))) {
	    b_solve = true;
	    e->in_S = true;
	    //	    G.remove_edge( e );
	    //	    cerr << eid << " found, breaking...\n";
	    ++sizeS;
	    break;
	 }	    
	 ++eid;
      }
      
   } while (b_solve);

   //Next step: remove all Triangle-redundant edges
   for (auto e = G.E.begin();
	e != G.E.end();
	++e) {
      if ( !e->in_S ) {
	 if ( G.is_cycle_redundant( e ) ) {
	    e->in_W = true;
	 }
      }
   }

   for (auto i = G.V.begin();
	i != G.V.end();
	++i) {
      i->in_A = false;
      i->in_C = false;
   }
   
   //Run Bipartite-complement
   vector< pedge > R;
   G.bipartite_complement( R );
   G.logg(DEBUG, "Size of R:" + to_string(R.size()));

   // for (auto i = G.V.begin();
   // 	i != G.V.end();
   // 	++i) {
   //    i->in_A = false;
   //    i->in_C = false;
   // }
   
   // G.logg(DEBUG, "Starting bipartite...");
   // vector< pedge > B;
   // G.bipartite( B );
   // G.logg(DEBUG, "Size of G':" + to_string(B.size() + R.size()));

   for (auto i = R.begin();
	i != R.end();
	++i) {
      (*i)->in_S = true;
   }
   
   //   G.logg(INFO, "Solution size:" + to_string(R.size() + sizeS));

   return 0; //R.size() + sizeS;
}

/*
 * CARL algorithm (k - 1/2)-approx
 * Only for odd cycle lengths k
 */
unsigned carl( Graph& G, size_t k, unsigned nThreads = 18, bool bVerb = false ) {
   G.logg << "Starting CARL..." << endL;
   
   if (k % 2 == 0) {
      G.logg << ERROR << "CARL can only be used with odd cycles.";
      return 0;
   }

   G.list_cycles( k, true );
   
   vector< double > lpsol;
   IloEnv env;
   IloCplex cplex( env );

   bool status = solve_lp( G, lpsol, env, cplex, nThreads, bVerb );
   if (status == false)
      return 0;
   
   unsigned eid = 0;
   vector< pedge > W;

   for (pedge e = G.E.begin();
	e != G.E.end();
	++e ) {
      if ( lpsol[eid] < 1.0 / (2*k - 1) ) {
	 e-> in_W = true;
	 W.push_back( e );
	 //	 lpsol[eid] = 0.0;
	    
	 if ( !(e-> in_S) ) {

	    //ADD other edges of triangles containing e
	    
	    for ( auto it = e->Delta.begin(); it != e->Delta.end(); ++it ) {
	       if (G.cycle_valid(*it)) {
		  pedge eToAdd;
		  //put in edge from this triangle with max weight

		  double maxWeight = 0;
		  pcycle& T = *it;
		  for ( size_t j = 0; j < T->cyEdges.size(); ++j ) {
		     if ( lpsol[ T->cyEdges[j]->eid ] > maxWeight ) {
			eToAdd = T->cyEdges[j];
			maxWeight = lpsol[ T->cyEdges[j]->eid ];
		     }
		  }
		  eToAdd->in_S = true;
		  
	       }
	    }
	 }
      }
     
      ++eid;
   }

   G.logg(DEBUG, "Starting bipartite (complement)...");
   vector< pedge > R;
   G.bipartite_complement( R );
   G.logg(DEBUG, "Size of R:" + to_string(R.size()));

   for (auto i = R.begin();
	i != R.end();
	++i) {
      (*i)->in_S = true;
   }
   
   return G.countS();
}


#endif
