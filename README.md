
# Triangle Interdiction

Provided source code for DART1, DART2, TARL, KORTSARZ, and the primal-dual algorithm
to approximate the TIP problem. Code for DART1,DART2,PRIMAL-DUAL is in "mygraph.cpp", while
TARL and KORTSARZ are implemented in "glpk_solver.cpp". An example program demonstrating
the algorithms is provided in "main_glpk.cpp".

## Dependencies
- the GLPK library: https://www.gnu.org/software/glpk/
  On debian-based systems, GLPK can be installed via
  the following command: `sudo apt-get install libglpk-dev`
- the GNU C++ compiler (g++), pthread library, and GNU make utility

Compilation instructions:
Simply type `make` in the root directory of the source code,
which produces compiled program `tip`. 

## Dataset
### Sample Datasets
1. Gnutella dataset (from Stanford Network Analysis Project (SNAP), see paper)
   ./datasets/p2p-Gnutella08.txt

### Format
The primary input file format is a simple, undirected edge-list format in plain text,
with the number of nodes (max. node id) on the first line, and each undirected edge
listed on the following lines.

## Usage
Example usage:
```
	To run DART1 on Youtube:
	./tip -G ./datasets/com-youtube.ungraph.txt -D
	To run DART2 on Youtube:	    
	./tip -G ./datasets/com-youtube.ungraph.txt -E
        To run TARL on Gnutella:	    
	./tip -G ./datasets/p2p-Gnutella08.txt -T
	To run KORTSARZ on Gnutella:	    
	./tip -G ./datasets/p2p-Gnutella08.txt -K
	To run PRIMAL-DUAL on Gnutella:	    
	./tip -G ./datasets/p2p-Gnutella08.txt -P
```

Explanation of output:
tip will log information to the standard output as it runs. Once the specified
algorithm completes, it will report the size of solution S, the running time in seconds,
and the peak memory usage in MB.
	  
```
Options:
-G <graph filename in edge list format>
-g <graph filename in binary edge list format>
-D [run DART1]
-E [run DART2 (second implementation)]
-F [run DARTP (parallelized implementation)]
-T [run TARL]
-K [run 2-approx. of Kortsarz et al.]
-O [run optimal solution via GNU GLPK]
-P [run primal-dual algorithm]
-p [Preprocess graph and count triangles only]
-A <m_add> (run DART, then adaptively add <m_add> random edges to the network)
-R <m_remove> (run DART, then adaptively remove <m_remove> random edges from the network)
-S <m_addremove> (run DART, then adaptively add <m_addremove> random edges to the network, and then remove the same edges)
-t [time limit in hours, default is 4]
-x [maximum number of threads]]
```
# Arbitrary Cycle Transversal

## Dependencies
As for `tip`, but in addition the `IBM CPLEX` library: to obtain a copy of this
software, contact [IBM](https://www.ibm.com/products/ilog-cplex-optimization-studio).
## Compilation
Update the Makefile with the location of the `IBM CPLEX` library. Then `make ect` compiles
binary `ect`.
## Usage
Similar to `tip`.
```
Options:
-G <graph filename in edge list format>
-g <graph filename in binary edge list format>
-k <cycle length>
-D [run DARC]
-K [run KORTSARZ]
-C [run CARL]
-O [run OPT]
-P [run PRIMALDUAL]
-t [time limit in hours, default is 4]
-x [maximum number of threads]]
```