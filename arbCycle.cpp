#include <iostream>
#include "ArbCycleGraph.cpp"
//#include "glpk_solver.cpp"
#include <string>
#include <iomanip>
#include <ctime>
#include <unistd.h>
#include <random>
#include "proc_size.cpp"
#include <chrono>

using namespace arbCycleGraph;

enum Algo {DART1, DART2, OPT};

void outputFile( ostream& os,
		 string& fname,
		 string algName,
		 unsigned n,
		 unsigned m,
		 double preprocessTime,
		 unsigned solSize,
		 double runningTime ) {
  os << fname;
  os << ' ';
  os << algName << ' ';
  os << n << ' ';
  os << m << ' ';
  os << preprocessTime << ' ';
  os << solSize << ' ';
  os << runningTime << ' ';
  os << getPeakRSS() / (1024.0 * 1024.0) << endl; //peak resources used in Mb
}
		 

void print_help() {
  cout << "Options: " << endl;
  cout << "-G <graph filename in edge list format>" << endl
       << "-g <graph filename in binary edge list format>" << endl
       << "-A <m_add> (run DART, then adaptively add <m_add> random edges to the network)" << endl
       << "-R <m_remove> (run DART, then adaptively remove <m_remove> random edges from the network)" << endl
       << "-S <m_addremove> (run DART, then adaptively add <m_addremove> random edges to the network, and then remove the same edges)" << endl
       << "-t [time limit in hours, default is 4]" << endl
       << "-x [maximum number of threads]]" << endl
       << "-C [Perform in-depth comparison of dynamic algorithms]" << endl;
   
}

using namespace std;
using namespace mygraph;

int main(int argc, char ** argv) {
  int c;
  extern char *optarg;
  //  extern int optint, optopt;

  if (argc == 1) {
     print_help();
     return 1;
  }

  string fname;
  
  string s_arg;
  double max_hours = 4.0;
  string outfilename;
  unsigned erN = 0;
  double erP;
  bool bBinaryFormat = false;
  
  while ((c = getopt( argc, argv, ":G:g:") ) != -1) {
    switch(c) {
    case 'G':
      //graph specification
      fname.assign( optarg );
      
      break;
    case 'g':
       fname.assign( optarg );
       bBinaryFormat = true;
       break;
    case '?':
      print_help();
      return 1;
      break;
    }
  }
  
  if (fname.size() == 0) {
     print_help();
     cerr << "Input graph is required.\n";
     return 1;
  } else {
    if (fname.substr(0, 2) == "ER") {
      size_t pos_colon = fname.find_first_of( ':' );
      string s_n = fname.substr(2, (pos_colon - 2));
      string s_p = fname.substr( pos_colon + 1 );
      erP = stod( s_p );
      erN = stoi( s_n );
    }
  }

  Graph G(INFO, cout);
  G.logg(INFO, "Reading graph...");
  if (bBinaryFormat) {
     G.read_edge_list_bin( fname );
  } else {
     if (erN > 0) {
	//generate ER graph
	G.genER( erN, erP );
     } else {
	G.read_edge_list( fname );
     }
  }
	
  G.logg(INFO, "Basic graph info (n, m): " + to_string( G.V.size() ) + " "  + to_string( G.E.size() ) );
  
  return 0;
}

