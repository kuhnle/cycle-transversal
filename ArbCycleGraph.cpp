#ifndef ARBCYCLEGRAPH_CPP
#define ARBCYCLEGRAPH_CPP

#include <queue>
#include <list>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <unordered_set>
#include <thread>
#include <mutex>
#include <iomanip>
#include <cmath>
#include "logger.cpp"

namespace arbCycleGraph {
   using namespace std;

   double elapsedTime( clock_t& t_start ) {
      return double(clock() - t_start) / CLOCKS_PER_SEC;
   }

   template<typename T>
   void print_vector( vector< T >& v, ostream& os = cout ) {
      for (size_t i = 0; i < v.size(); ++i) {
	 os << v[i] << ' ';
      }
      os << endl;
   }

   template <typename T, typename Compare>
   std::vector<std::size_t> sort_permutation(
					     const std::vector<T>& vec,
					     Compare& compare)
   {
      std::vector<std::size_t> p(vec.size());
      std::iota(p.begin(), p.end(), 0);
      std::sort(p.begin(), p.end(),
		[&](std::size_t i, std::size_t j){ return compare(vec[i], vec[j]); });
      return p;
   }

   template <typename T>
   void apply_permutation_in_place(
				   std::vector<T>& vec,
				   const std::vector<std::size_t>& p)
   {
      std::vector<bool> done(vec.size());
      for (std::size_t i = 0; i < vec.size(); ++i)
	 {
	    if (done[i])
	       {
		  continue;
	       }
	    done[i] = true;
	    std::size_t prev_j = i;
	    std::size_t j = p[i];
	    while (i != j)
	       {
		  std::swap(vec[prev_j], vec[j]);
		  done[j] = true;
		  prev_j = j;
		  j = p[j];
	       }
	 }
   }





   random_device rd;
   mt19937 gen( rd() );
  
   typedef unsigned node_id;
   bool mycompare( const node_id& a, const node_id& b ) {
      return a < b;
   }  
   class Edge;
   class Cycle;

   typedef list< Edge >::iterator pedge; //this is how we will access edges
   typedef list< Cycle >::iterator pcycle;
  

   class Graph;
   
   class Segment {
   public:
      vector< node_id > V;
      vector< int > Index; //index of V[i] in V[i - 1]'s adjacency list
      size_t length;
      vector< bool > chosen;
      
      Segment( node_id s, size_t l );
      bool exhFillOutSegment( size_t pos, Graph& G );
      bool exhaustiveNextSegment( Graph& G );
      bool fillOutSegment( size_t pos, Graph& G );
      bool nextSegment( Graph& G );
      node_id last();

      friend std::ostream& operator<< ( std::ostream& os, const Segment& S ) {
	 for (size_t i = 0; i < S.V.size(); ++i) {
	    os << S.V[ i ] << ' ';
	 }
	 
	 return os;
      }
      
   };

   class Cycle {
   public:
      vector< pedge > cyEdges;

      Cycle( ) {

      }

      Cycle( vector< pedge >& inEdges ) {
	 cyEdges.assign( inEdges.begin(), inEdges.end());
      }

      Cycle( const Cycle& rhs ) {
	 cyEdges.assign( rhs.cyEdges.begin(), rhs.cyEdges.end());
      }

      Cycle& operator=( const Cycle& rhs ) {
	 cyEdges.assign( rhs.cyEdges.begin(), rhs.cyEdges.end());
	 
	 return *this;
      }
   };
   
  
   // bool operator==( const Triangle& lhs, const Triangle& rhs ) {
   //    bool b1 = (lhs.e1 == rhs.e1) || (lhs.e1 == rhs.e2) || (lhs.e1 == rhs.e3);
   //    bool b2 = (lhs.e2 == rhs.e1) || (lhs.e2 == rhs.e2) || (lhs.e2 == rhs.e3);
   //    bool b3 = (lhs.e3 == rhs.e1) || (lhs.e3 == rhs.e2) || (lhs.e3 == rhs.e3);

   //    return b1 && b2 && b3;
   // }

   // bool operator!=( const Triangle& lhs, const Triangle& rhs ) {
   //    return !(lhs == rhs);
   // }
   
   class Edge {
   public:
      node_id from; //node id's
      node_id to;
     
      node_id eid;
      list< pcycle > Delta; //the cycles containing this edge
      bool in_S; //whether this edge is selected for membership in S
      bool in_W; //whether this edge is selected for membership in W
      pcycle C_e; //If this edge e is included in S, this cycle is h(e) (see paper)

      void print( ostream& os ) {
	 os << "(" << from << "," << to << ")";
      }
      
      //Returns an incident vertex not equal to the input id
      node_id other( node_id one ) {
	 if (from != one)
	    return from;
	 else
	    return to;
      }
    
      bool incident( node_id id ) {
	 return ( (from == id) || (to == id) );
      }

      Edge( node_id in_from, node_id in_to ) {
	 in_S = false;
	 in_W = false;
	 from = in_from;
	 to = in_to;
      }
   };
  
   /*
    * Vertex class
    */
   class Node {
   public:
      //    node_id id; Can allow id to be index in vector
      //for dynamic graph
      list< pedge > neighbors; //modified adjacency list
      list< node_id > nei_ids;
      
      //for static graph
      vector< pedge > v_neighbors;
      vector< node_id > v_nei_ids;
      
      bool in_A; // for bipartite procedure
      bool in_C; // for bipartite procedure
      Node() { in_A = false; in_C = false;
      }

      void insert_sort( pedge& e, node_id myId ) {
	 node_id other_e = e->other( myId );
	 node_id other_nei;
	 
	 auto it = neighbors.begin();
	 auto it2 = nei_ids.begin();
	 while (it != neighbors.end()) {
	    other_nei = (*it)->other( myId );
	    if (other_e > other_nei) {
	       ++it;
	       ++it2;
	    }
	    else
	       break;
	 }
	 neighbors.insert( it, e );
	 nei_ids.insert( it2, other_e );
      }
      
      void insert( pedge& e, node_id myId, node_id otherId ) {
	 //	 neighbors.push_back( e );
	 v_neighbors.push_back( e );
	 //	 cerr << "Adding " << otherId << " to " << myId << "'s list" << endl;
	 this->v_nei_ids.push_back( otherId );
	 //	 print_vector( v_nei_ids, cerr );
      }

      void nid_insert( node_id nei_id ) {
	 v_nei_ids.push_back( nei_id );
      }

      vector< pedge >::iterator incident_static2( node_id id ) {
	 vector< pedge >::iterator it1 = v_neighbors.begin();
	 while (it1 != v_neighbors.end()) {
	    if ( (*(*it1)).incident( id ) ) {
	       return it1;
	    }
	    ++it1;
	 }

	 return it1;
      }
      
      bool incident( node_id id ) {
	 list< pedge >::iterator it1 = neighbors.begin();
	 while (it1 != neighbors.end()) {
	    if ( (*(*it1)).incident( id ) ) {
	       return true;
	    }
	    ++it1;
	 }

	 return false;
      }

      bool incident_static( node_id id ) {
	 vector< pedge >::iterator it1 = v_neighbors.begin();
	 while (it1 != v_neighbors.end()) {
	    if ( (*(*it1)).incident( id ) ) {
	       return true;
	    }
	    ++it1;
	 }

	 return false;
      }

      Node( const Node& in ) {
	 list< pedge >::const_iterator it1 = in.neighbors.begin();
	 while (it1 != in.neighbors.end()) {
	    neighbors.push_back( *it1 );
	    ++it1;
	 }
	 in_A = in.in_A;
	 in_C = in.in_C;
	 v_nei_ids.assign( in.v_nei_ids.begin(), in.v_nei_ids.end() );
	 v_neighbors.assign( in.v_neighbors.begin(), in.v_neighbors.end() );
	 nei_ids.assign( in.nei_ids.begin(), in.nei_ids.end() );
      }

      Node& operator=( const Node& in ) {
	 neighbors.clear();
	 list< pedge >::const_iterator it1 = in.neighbors.begin();
	 while (it1 != in.neighbors.end()) {
	    neighbors.push_back( *it1 );
	    ++it1;
	 }

	 in_A = in.in_A;
	 in_C = in.in_C;
	 v_nei_ids.assign( in.v_nei_ids.begin(), in.v_nei_ids.end() );
	 v_neighbors.assign( in.v_neighbors.begin(), in.v_neighbors.end() );
	 nei_ids.assign( in.nei_ids.begin(), in.nei_ids.end() );
	 return *this;
      }

    
   };

   /*
    * For now, this only represents UNdirected graphs
    */
  
   class Graph {
   public:
      node_id n; //number of nodes
      node_id m; //number of edges
      vector< Node > V; //set of vertices
      list< Edge > E;   //set of edges
      vector< Edge > vE;
      list< Cycle > C;   //cycle list
      list< Cycle > Csol;   //list of cycles in solution of DART
      Logger logg;
      unsigned sizeS;
      double runningTime;
      double tCycles;
      double preprocessTime;
    
      void save( ostream& os ) {
	 
      }
     
      Graph( const Graph& H ) {
	 n = H.n;
	 m = 0;
	 init_empty_graph();
	 for (auto it = H.E.begin();
	      it != H.E.end();
	      ++it) {
	    add_edge(it->from, it->to);
	 }

	 clock_t t_start = clock();
	 for ( auto v = V.begin(); v != V.end(); ++v ) {
	    auto p = sort_permutation( v->v_nei_ids, mycompare );
	    apply_permutation_in_place( v->v_nei_ids, p );
	    apply_permutation_in_place( v->v_neighbors, p );
	 }
	 preprocessTime = double (clock() - t_start) / CLOCKS_PER_SEC;
      }
     
      Graph() {
	 n = 0;
	 m = 0;
      }

      Graph( LogType LT, ostream& os ) : logg( LT, os ) {
	 n = 0;
	 m = 0;
      }

      /*
       * Writes graph as graphml
       * Edges in S are red
       * Edges in W are blue (if show_pruned)
       */
      void write_graphml( ofstream& of, bool show_pruned = false ) {
	 //header info
	 of << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	 of << "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\">\n";

	 //the color attribute
	 of << "<key id=\"d0\" for=\"edge\" attr.name=\"color\" attr.type=\"string\">\n"
	    << "<default>gray</default>\n"
	    << "</key>\n";
	 //the size attribute
	 of << "<key attr.name=\"size\" attr.type=\"float\" for=\"node\" id=\"size\"/>";
	 //edge weight attribute
	 of << "<key attr.name=\"weight\" attr.type=\"double\" for=\"edge\" id=\"weight\"/>\n";
	 //Start writing graph
	 of << "<graph id=\"G\" edgedefault=\"undirected\">\n";
	 //nodes
	 unsigned n = V.size();
	 double nsize;
	 for (unsigned i = 0; i < n; ++i) {
	    string nid = to_string( i );
	    of << "<node id=\"" << nid << "\">\n";
	    //give this node a fixed, large size
	    nsize = 15.0;
	    of << "<data key=\"size\">" << nsize << "</data>\n";
	    of << "</node>\n";
	 }
 

	 //edges
	 unsigned from;
	 unsigned to;
	 unsigned eid = 0;
	 for (auto it = E.begin(); it != E.end(); ++it ) {
	    from = it->from;
	    to = it-> to;
	    string seid = to_string( eid );
	    seid = "e" + seid;
	    of << "<edge id=\"" << seid << "\" source=\"" << to_string( from ) << "\" target=\"" << to_string( to ) << "\">\n";
	    //write the edge weight
	    of << "<data key=\"weight\">" << 1.0 << "</data>\n";
	    if (it->in_S) {
	       //make these edges red
	       of << "<data key=\"d0\">red</data>\n";
	    }
	    if (show_pruned) {
	       if (it->in_W) {
		  //make these edges blue
		  of << "<data key=\"d0\">blue</data>\n";
	       }
	    }
	    of << "</edge>\n";
	 }
      
	 //finish up
	 of << "</graph>\n</graphml>\n";
      }

    
      //     Graph( const Graph& in ) : logg( in.logg.loglevel, in.logg.of ) {
	
      //     }

      /*
       * Prepares data structure for efficient insertion, deletion of edges
       * By using std::list for neighboring edges in each node,
       * instead of vector
       */
      void init_dynamic( bool b_delete_static_info = false ) {
	 for (size_t i = 0; i < V.size(); ++i ) {
	    Node& node = V[i];
	    node.neighbors.assign( node.v_neighbors.begin(), node.v_neighbors.end() );
	    node.nei_ids.assign( node.v_nei_ids.begin(), node.v_nei_ids.end() );
	    if (b_delete_static_info) {
	       node.v_neighbors.clear();
	       node.v_nei_ids.clear();
	    }
	 }
      }

      /*
       * Prepares data structure for efficient insertion, deletion of edges
       * By using std::list for neighboring edges in each node,
       * instead of vector
       */
      void init_static( ) {
	 for (node_id i = 0; i < V.size(); ++i) {
	    Node& node = V[i];
	    node.v_neighbors.assign( node.neighbors.begin(), node.neighbors.end() );
	    node.v_nei_ids.clear();
	    for (auto it1 = node.neighbors.begin();
		 it1 != node.neighbors.end();
		 ++it1) {
	       node.v_nei_ids.push_back( (*it1)->other( i ) );
	    }
	 }
      }
      
      void clear_edges() {
	 for (auto i = E.begin();
	      i != E.end();
	      ++i) {
	    i->in_S = false;
	    i->in_W = false;
	 }
      }

      void clear_graph() {
	 n = 0;
	 m = 0;
	 V.clear();
	 E.clear();
	 vE.clear();
	 C.clear();
	 Csol.clear();
	 runningTime = 0.0;
	 preprocessTime = 0.0;
	 sizeS = 0;
      }
     
      void init_empty_graph() {
	 Node tmp;
	 V.clear();
	 V.assign(n, tmp );
	 //	 for (unsigned i = 0; i < n; ++i) {
	 //	    V.push_back( tmp );
	 //
	 //	 }
      }

      void print_graph( ostream& os ) {
	 os << V.size() << ' ' << E.size() << endl;
	 for (size_t i = 0; i < V.size(); ++i) {
	    os << i << endl;
	    for (auto e = V[i].neighbors.begin();
		 e != V[i].neighbors.end();
		 ++e ) {
	       (*e)->print(os);
	       os << ' ';
	    }
	    os << endl;
	    for (unsigned j = 0; j < V[i].v_nei_ids.size(); ++j) {
	       os << V[i].v_nei_ids[j] << ' ';
	    }
	    os << endl;
	 }
      }
      
      void bipartite( vector< pedge>& B ) {
	 unsigned nC;
	 unsigned nA;
	 node_id this_node = 0;
	 for (auto i = V.begin();
	      i != V.end();
	      ++i) {
	    nC = 0;
	    nA = 0;
	    for (auto e = i->v_neighbors.begin();
		 e != i->v_neighbors.end();
		 ++e) {
	       if ( !( (*e)->in_S || (*e)->in_W ) ) { //Ignore edges in S or W
		  node_id other = (*e)->other( this_node );
		  if (V[other].in_A)
		     ++nA;
		  if (V[other].in_C)
		     ++nC;
	       }
	    }

	    if (nA >= nC) {
	       i->in_C = true;
	    } else {
	       i->in_A = true;
	    }
	
	    ++this_node;
	 }

	 //return the edges that cross the partition
	 B.clear();
	 for (pedge e = E.begin();
	      e != E.end();
	      ++e ) {
	    if ( !( e->in_S || e->in_W ) ) { //Ignore edges in S or W
	       if ( V[ e->from ].in_A ) {
		  if ( V[ e->to ].in_C ) {
		     //e crosses
		     B.push_back( e );
		  }
	       } else {
		  if ( V[ e->to ].in_A ) {
		     //e crosses
		     B.push_back( e );
		  }
	       }
	    
	    }
	 }
      }

      /*
       * Returns complement of 'bipartite'
       */
      void bipartite_complement( vector< pedge>& R ) {
	 unsigned nC;
	 unsigned nA;
	 node_id this_node = 0;
	 for (auto i = V.begin();
	      i != V.end();
	      ++i) {
	    nC = 0;
	    nA = 0;
	    for (auto e = i->v_neighbors.begin();
		 e != i->v_neighbors.end();
		 ++e) {
	       if ( !( (*e)->in_S || (*e)->in_W ) ) { //Ignore edges in S or W
		  node_id other = (*e)->other( this_node );
		  if (V[other].in_A)
		     ++nA;
		  if (V[other].in_C)
		     ++nC;
	       }

	    }

	    if (nA >= nC) {
	       i->in_C = true;
	    } else {
	       i->in_A = true;
	    }
	
	    ++this_node;
	 }

	 //return the edges that cross the partition
	 R.clear();
	 for (pedge e = E.begin();
	      e != E.end();
	      ++e ) {
	    if ( !( e->in_S || e->in_W ) ) { //Ignore edges in S or W
	       if ( V[ e->from ].in_A ) {
		  if ( V[ e->to ].in_A ) {
		     //e crosses
		     R.push_back( e );
		  }
	       } else {
		  if ( V[ e->to ].in_C ) {
		     //e crosses
		     R.push_back( e );
		  }
	       }
	    }
	 }
      }

      void clear_cycles() {
	 C.clear();
	 for (auto it = E.begin();
	      it != E.end();
	      ++it) {
	    it->Delta.clear();
	 }
      }

      // void add_triangle(node_id& v, size_t& v_s, size_t& v_t, size_t& s_t,
      // 			node_id& s, node_id& t ) {
      // 	 //get edges (v,s), (v,t), (s,t)
      // 	 pedge& vs = V[ v ].v_neighbors[ v_s ];
      // 	 pedge& vt = V[ v ].v_neighbors[ v_t ];
      // 	 pedge& st = V[ s ].v_neighbors[ s_t ];

      // 	 Triangle T_out;
      // 	 T_out.e1 = vs;
      // 	 T_out.e2 = vt;
      // 	 T_out.e3 = st;

      // 	 T.push_front(T_out);
      // 	 pangle tt = T.begin();
      // 	 vs->Delta.push_back( tt );
      // 	 vt->Delta.push_back( tt );
      // 	 st->Delta.push_back( tt );
      // }

      void list_cycles( size_t k, bool list = false ) {
	 logg << INFO << "Listing all cycles of length " << k << endL;
	 
	 clock_t t_start = clock();

	 size_t count = 0;
	 
	 for (node_id s = 0; s < n; ++s ) {
	    //Find all cycles with smallest node s
	    Segment CC (s, k - 2 ); //segment with k - 1 edges has k nodes
	    while ( CC.nextSegment( *this ) ) {
	       //cerr << CC << endl;
	       
	       //Have produced a valid segment
	       //With smallest vertex s
	       //In increasing order
	       //Now need to find all common
	       //neighbors c of s,t
	       //with c > s, c > t.
	       node_id t = CC.last();
	       count += findAllCycles( s, t, CC, list );
	    }
	 }
	 tCycles = double (clock() - t_start) / CLOCKS_PER_SEC;
	 logg << INFO << count << " " << k << "-cycles found." << endL;
	 
      }

      void addCycle( Segment& CC, size_t& sPos, size_t& tPos ) {
	 node_id& s = CC.V.front();
	 const node_id& t = CC.last();

	 Cycle C_out;
	 
	 vector< pedge >& cyEdges = C_out.cyEdges;
	 cyEdges.reserve( CC.length + 2 );
	 cyEdges.push_back( V[ s ].v_neighbors[ sPos ] );
	 cyEdges.push_back( V[ t ].v_neighbors[ tPos ] );
	 for ( size_t i = 1; i <= CC.length; ++i ) {
	    cyEdges.push_back( V[ CC.V[ i - 1 ] ].v_neighbors[ CC.Index[ i ] ] );
	 }

	 C.push_front( C_out );
	 pcycle cc = C.begin();
	 for (size_t i = 0; i < cyEdges.size(); ++i) {
	    cyEdges[i]->Delta.push_back( cc );
	 }
      }

      bool cycle_valid( pcycle& c ) {
	 for (size_t i = 0; i < c->cyEdges.size(); ++i) {
	    if ( c->cyEdges[ i ]->in_S )
	       return false;

	 }

	 return true;
      }

      size_t countCvalid() {
	 size_t count = 0;
	 for (auto c = C.begin();
	      c != C.end();
	      ++c) {
	    if (cycle_valid( c ))
	       ++count;
	 }

	 return count;
      }

      bool is_cycle_redundant( pedge& e ) {
	 bool r = true;
	 for (auto i = e->Delta.begin();
	      i != e->Delta.end();
	      ++i ) {
	    if ( cycle_valid(*i) ) {
	       r = false;
	       break;
	    }
	 }

	 return r;
      }
      
      void darcFindAllCycles( node_id& s, node_id& t, Segment& CC ) {
	 //Know that s < t
	 //Find first neighbor c of s, c > t
	 //Know that s has at least one neighbor (same with t)
	 size_t sPos = 0;
	 vector< node_id >& sNeis = V[ s ].v_nei_ids;
	 while ( sNeis[ sPos ] < t ) {
	    ++sPos;
	    if (sPos >= sNeis.size())
	       return;
	 }

	 //Find first neighbor d of t, d > t
	 size_t tPos = 0;
	 vector< node_id >& tNeis = V[ t ].v_nei_ids;
	 while ( tNeis[ tPos ] < t ) {
	    ++tPos;
	    if (tPos >= tNeis.size())
	       return;
	 }

	 //Now find all common neighbors
	 node_id sNei = sNeis[ sPos ];
	 node_id tNei = tNeis[ tPos ];
	 vector< pedge > emptyEdges( CC.length + 2, E.begin() );
	 Cycle cyc( emptyEdges ) ;
	 while (1) {
	    if ( sNei < tNei ) {
	       ++sPos;

	       if (sPos >= sNeis.size())
		  break;
	       sNei = sNeis[ sPos ];
	    } else {
	       if (tNei < sNei) {
		  ++tPos;
		  if (tPos >= tNeis.size())
		     break;
		  tNei = tNeis[ tPos ];
	       } else {
		  //Found cycle

		  if ( cycle_disjoint( cyc, CC, sPos, tPos ) ) {
		     Csol.push_front( cyc );
		     pcycle c = Csol.begin();
		     cycle_add( c );
		  }
	       
		  ++sPos;
		  if (sPos >= sNeis.size())
		     break;
		  sNei = sNeis[ sPos ];

		  ++tPos;
		  if (tPos >= tNeis.size())
		     break;
		  tNei = tNeis[ tPos ];
	       }
	    }
	 }
	 
      }

      bool pruneFindCycles( node_id& s, node_id& t, Segment& CC ) {
	 //find first neightbor c of s
	 size_t sPos = 0;
	 vector< node_id >& sNeis = V[ s ].v_nei_ids;
	 
	 //Find first neighbor d of t
	 size_t tPos = 0;
	 vector< node_id >& tNeis = V[ t ].v_nei_ids;

	 //Now find all common neighbors
	 node_id sNei = sNeis[ sPos ];
	 node_id tNei = tNeis[ tPos ];
	 vector< pedge > emptyEdges( CC.length + 2, E.begin() );
	 Cycle cyc( emptyEdges ) ;
	 while (1) {
	    if ( sNei < tNei ) {
	       ++sPos;

	       if (sPos >= sNeis.size())
		  break;
	       sNei = sNeis[ sPos ];
	    } else {
	       if (tNei < sNei) {
		  ++tPos;
		  if (tPos >= tNeis.size())
		     break;
		  tNei = tNeis[ tPos ];
	       } else {
		  //Found cycle (unless sNei is repeated...)
		  bool bRepeat = false;
		  for (size_t i = 0; i < CC.V.size(); ++i) {
		     if ( CC.V[i] == sNei ) {
			bRepeat = true;
			break;
		     }
		  }
		  if (!bRepeat) {
		     if ( cycle_disjoint( cyc, CC, sPos, tPos ) ) {
			cerr << "Found disjoint cycle: " << CC << sNei << endl;
		     
			return false;
		     }
		  }
	       
		  ++sPos;
		  if (sPos >= sNeis.size())
		     break;
		  sNei = sNeis[ sPos ];

		  ++tPos;
		  if (tPos >= tNeis.size())
		     break;
		  tNei = tNeis[ tPos ];
	       }
	    }
	 }
	 
	 return true; //edge is prunable so far
      }
      
      
      size_t findAllCycles( node_id& s, node_id& t, Segment& CC, bool list = false ) {
	 size_t foundCycles = 0;
	 //Know that s < t
	 //Find first neighbor c of s, c > t
	 //Know that s has at least one neighbor (same with t)
	 size_t sPos = 0;
	 vector< node_id >& sNeis = V[ s ].v_nei_ids;
	 while ( sNeis[ sPos ] < t ) {
	    ++sPos;
	    if (sPos >= sNeis.size())
	       return 0;
	 }

	 //Find first neighbor d of t, d > t
	 size_t tPos = 0;
	 vector< node_id >& tNeis = V[ t ].v_nei_ids;
	 while ( tNeis[ tPos ] < t ) {
	    ++tPos;
	    if (tPos >= tNeis.size())
	       return 0;
	 }

	 //Now find all common neighbors
	 node_id sNei = sNeis[ sPos ];
	 node_id tNei = tNeis[ tPos ];
	 while (1) {
	    if ( sNei < tNei ) {
	       ++sPos;

	       if (sPos >= sNeis.size())
		  break;
	       sNei = sNeis[ sPos ];
	    } else {
	       if (tNei < sNei) {
		  ++tPos;
		  if (tPos >= tNeis.size())
		     break;
		  tNei = tNeis[ tPos ];
	       } else {
		  //Found cycle
		  ++foundCycles;
		  if (list)
		     addCycle( CC, sPos, tPos );
		  //cout << "Found cycle: " << CC << sNei << endl;
	       
		  ++sPos;
		  if (sPos >= sNeis.size())
		     break;
		  sNei = sNeis[ sPos ];

		  ++tPos;
		  if (tPos >= tNeis.size())
		     break;
		  tNei = tNeis[ tPos ];
	       }
	    }
	 }
	 
	 return foundCycles;
      }

      unsigned countS() {
	 unsigned count = 0;
	 for (auto i = E.begin();
	      i != E.end();
	      ++i) {
	
	    if (i->in_S)
	       ++count;
	 }
	 sizeS = count;
	 return count;
      }

      bool cycle_disjoint( const pcycle& c ) {
	 vector< pedge >& cEdges = c->cyEdges;

	 for (size_t i = 0; i < cEdges.size(); ++i) {
	    if ( cEdges[i] -> in_S )
	       return false;
	 }

	 return true;
      }

      bool cycle_disjoint( Cycle& c, Segment& CC, size_t& sPos, size_t& tPos ) {
	 node_id& s = CC.V.front();
	 const node_id& t = CC.last();

	 vector< pedge >& cyEdges = c.cyEdges;
	 cyEdges[0] = V[ s ].v_neighbors[ sPos ];
	 cyEdges[1] = V[ t ].v_neighbors[ tPos ];
	 for ( size_t i = 1; i <= CC.length; ++i ) {
	    cyEdges[i + 1] = V[ CC.V[ i - 1 ] ].v_neighbors[ CC.Index[ i ] ];
	 }
	 
	 vector< pedge >& cEdges = c.cyEdges;

	 for (size_t i = 0; i < cEdges.size(); ++i) {
	    if ( cEdges[i] -> in_S )
	       return false;
	 }

	 return true;
      }

      void listAllTuples( vector< vector< node_id > >& v, size_t k ) {
	 if (k == 1) {
	    for (size_t i = 0; i < n; ++i) {
	       vector< node_id > tmp( 1, i );
	       v.push_back( tmp );
	    }

	    return;
	 }
	 
	 listAllTuples( v, k - 1 );
	 vector< vector< node_id > > w;
	 for (size_t i = 0; i < v.size(); ++i) {
	    vector< node_id >& curr = v[ i ];
	    node_id cc = curr.back() + 1;
	    while (cc < n) {
	       vector< node_id > newCurr( curr.begin(), curr.end() );
	       newCurr.push_back( cc );

	       w.push_back( newCurr );
	       ++cc;
	    }
	 }
	 v.swap( w );
      }

      bool testCycle( vector< node_id >& c ) {
	 Graph& G = *this;
	 vector< pedge >::iterator it1 = G.V[ c.front() ].incident_static2( c.back() );
	 if (it1 == G.V[ c.front() ].v_neighbors.end()) {
	    return true; //passes bc not cycle
	 } else {
	    if ((*it1)->in_S) {
	       return true; //passes bc broken
	    }
	 }
	 for (size_t j = 0; j < c.size() - 1; ++j) {
	    it1 = G.V[ c[j] ].incident_static2( c[j + 1] );
	    if ( it1 == G.V[ c[j] ].v_neighbors.end() ) {
	       return true; //passes bc not cycle
	    }else {
	       if ((*it1)->in_S) {
		  return true; //passes bc broken
	       }
	    }
	 }

	 //Was cycle.
	 //no edge of which is in S
	 return false;
      }
      
      bool bruteTest( size_t k ) {
	 vector < vector < node_id > > v;
	 listAllTuples( v, k );
	 cout << "Tuples: " << v.size() << endl;
	 
	 for (size_t i = 0; i < v.size(); ++i) {
	    //print_vector( v[i] );
	    if (!testCycle( v[ i ] )) {
	       cout << "Cycle does not pass." << endl;
	       return false;
	    }
	 }
	 return true;
      }
      
      void cycle_add( pcycle& c ) {
	 vector< pedge >& cEdges = c->cyEdges;

	 for (size_t i = 0; i < cEdges.size(); ++i) {
	    cEdges[i] -> in_S = true;
	    cEdges[i] -> C_e = c;
	 }

      }

      bool cycleCounts( pcycle& c, pedge& e ) {
	 if (c == e->C_e)
	    return false;
	 vector< pedge >& edges = c->cyEdges;
	 for (size_t i = 0; i < edges.size(); ++i) {
	    if ( edges[i] -> in_S )
	       return false;
	 }

	 return true;
      }

      bool cycleTransversed( pcycle& c ) {
	 vector< pedge >& edges = c->cyEdges;
	 for (size_t i = 0; i < edges.size(); ++i) {
	    if ( edges[i]->in_S )
	       return true;
	 }

	 return false;
      }
      
      //void add( string name, size_t val ) {
      //zpstring sval = to_string( val );
      //data[ name ] = sval;
      //}

/*
       * This implementation uses the full cycle list
       * (prune will be slow otherwise, for large cycles...)
       */
      size_t darc_base( size_t k ) {
	 logg << INFO << "Starting DARC_BASE..." << endL;
	 size_t sizeS = 0;
	 list_cycles( k, true );

	 for (pcycle it = C.begin();
	      it != C.end();
	      ++it ) {
	    if ( cycle_disjoint( it ) ) {
	       cycle_add( it );
	       sizeS += k;
	    }
	 }

	 logg << "Before pruning (primal-dual): " << sizeS << endL;
	 logg << "PRUNE started..." << endL;
	 
	 //PRUNE phase
	 size_t n_pruned = 0;
	 for (auto e = E.begin();
	      e != E.end();
	      ++e) {
	    if (e->in_S) {
	       e->in_S = false;
	       if (!( cycleTransversed( e->C_e ))) {
		  e->in_S = true;
		  continue;
	       }
	       for (auto c = e->Delta.begin();
		    c != e->Delta.end();
		    ++c) {
		  if (cycleCounts( *c, e )) {
		     e->in_S = true;
		     break;
		  }
	       }
	       if (!e->in_S)
		  ++n_pruned;

	   
	    }
	 }

	 return sizeS - n_pruned;
      }

      //void add( string name, size_t val ) {
      //string sval = to_string( val );
      //data[ name ] = sval;
      //}

      /*
       * This implementation uses the full cycle list
       * 
       */
      size_t primal_dual( size_t k ) {
	 logg << INFO << "Starting PrimalDual..." << endL;
	 size_t sizeS = 0;
	 list_cycles( k, true );

	 for (pcycle it = C.begin();
	      it != C.end();
	      ++it ) {
	    if ( cycle_disjoint( it ) ) {
	       cycle_add( it );
	       sizeS += k;
	    }
	 }

	 logg << "Primal-dual: " << sizeS << endL;

	 return sizeS;
      }

      /*
       * This implementation only remembers cycles in the
       * solution (slower, but more space efficient)
       */
      void darc_base_free( size_t k ) {
	 logg << INFO << "Starting DARC_BASE_FREE (does not store cycle list)..." << endL;

	 //Initial phase
	 for (node_id s = 0; s < n; ++s ) {
	    //Find all cycles with smallest node s
	    Segment CC (s, k - 2 ); //segment with k - 1 edges has k nodes
	    while ( CC.nextSegment( *this ) ) {
	       //cerr << CC << endl;
	       
	       //Have produced a valid segment
	       //With smallest vertex s
	       //In increasing order
	       //Now need to find all common
	       //neighbors c of s,t
	       //with c > s, c > t.
	       node_id t = CC.last();
	       darcFindAllCycles( s, t, CC );
	    }
	 }
	 
	 logg << "PRUNE started..." << endL;
	 bool isPrunable;
	 for (node_id s = 0; s < n; ++s ) {
	    if (V[ s ].v_nei_ids.size() == 0)
	       continue;
	    size_t sPos = 0;
	    node_id t = V[ s ].v_nei_ids[ 0 ];
	    while ( s >= t ) {
	       ++sPos;
	       if (sPos >= V[ s ].v_nei_ids.size())
		  break;
	       t = V[ s ].v_nei_ids[ sPos ];
	    }
	    while (sPos < V[ s ].v_nei_ids.size()) {
	       t = V[ s ].v_nei_ids[ sPos ];
	       if ( V[ s ].v_neighbors[ sPos ]->in_S ) {
		  //Process this edge
		  cerr << "processing: " << s << ' ' << t << endl;
		  isPrunable = true;
		  //Prune it
		  V[ s ].v_neighbors[ sPos ]->in_S = false;
		  Segment CC (s, k - 2 ); //segment with k - 1 edges has k nodes
		  CC.chosen.assign( n, false );
		  CC.V[1] = t;
		  CC.Index[1] = sPos;
		  
		  if ( CC.exhFillOutSegment( 1, *this ) ) {
		     do {
			//Have produced a valid segment
			//Now need to find all common
			//neighbors c of s,t
		  
			node_id u = CC.last();
			if (!pruneFindCycles( s, u, CC ) ) {
			   //We cannot prune this edge (s,t)
			   isPrunable = false;
			   break;
			}
		     } while ( CC.exhaustiveNextSegment( *this ) );
		  } 

		  if (!isPrunable) {
		     V[ s ].v_neighbors[ sPos ]->in_S = true;
		  }
	       }
	       
	       ++sPos;
	    }
	 }
	 
      }

      /*
       * Input is an iterator to an element
       * in the edge list
       */
      void remove_edge( pedge& e_to_remove ) {
	 //remove from its incident nodes' adjacency list
	 node_id from = e_to_remove->from;
	 node_id to = e_to_remove->to;

	 //       cerr << "Removing " << from << ' ' << to << endl;
	 list< pedge >::iterator it2 = V[ from ].neighbors.begin();

	 while ((*it2)->other( from ) != to) {

	    ++it2;
	 }

	 V[ from ].neighbors.erase( it2 );

	 list< pedge >::iterator it3 = V[ to ].neighbors.begin();
      
	 while ((*it3)->other( to ) != from ) {

	    ++it3;
	 }
	 V[ to ].neighbors.erase( it3 );
       
	 E.erase( e_to_remove );
      }

      //Only works with undirected edges atm
      void add_edge( node_id from, node_id to ) {
	 if (from == to) { //don't allow self-loops
	    logg(WARN, "Self-loop (" + to_string( from ) + "," + to_string( to ) + "), not added to graph");
	    return;
	 }
	 if ( from >= n || to >= n ) {
	    //	logg(INFO, "Node index out of bounds: ignoring edge (" + to_string( from ) + "," + to_string( to ) + "), since n = " + to_string( n ) );

	    //augment graph to have enough nodes
	    n = from + 1;
	    if (to + 1 > n)
	       n = to + 1;

	    Node tmp;
	    while (V.size() < n) {
	       V.push_back( tmp );
	    }

	 }
      
	 Node& From = V[ from ];
	 Node& To = V[ to ];

	 //check if edge already exists
	 if ( From.incident_static( to ) ) {
	    logg(WARN, "Not adding edge (" + to_string( from ) + "," + to_string( to ) + "), since it already exists");
	    return;
	 }

	 //Need to add a new edge to the graph
	 //	 logg(DEBUG, "Adding edge (" + to_string( from ) + "," + to_string( to ) + ")");
	 Edge edge( from, to );
	 edge.eid = m;

	 E.push_back( edge );
	 pedge ee = --E.end();
	 From.insert( ee, from, to );
	 To.insert( ee, to, from );
	 
	 //	 From.nid_insert( to );
	 //	 To.nid_insert( from );
	 //	 From.v_neighbors.push_back( --E.end() );
	 //	 To.v_neighbors.push_back( --E.end() );
	 ++m; //number of edges has increased
      }

      bool verify_graph() {
	 for (node_id i = 0; i < n; ++i) {
	    auto it1 = V[i].neighbors.begin();
	    auto it2 = V[i].v_nei_ids.begin();
	    auto it3 = V[i].v_neighbors.begin();
	    if (V[i].neighbors.size() != V[i].v_nei_ids.size()) {
	       logg( ERROR, "Vertex " + to_string(i) );
	       logg( ERROR, "neighbors.size " + to_string( V[i].neighbors.size() )
		     + " v_nei_ids.size " + to_string( V[i].v_nei_ids.size())
		     + " v_neighbors.size " + to_string( V[i].v_neighbors.size()));
	       return false;
	    }
	    if (V[i].neighbors.size() != V[i].v_neighbors.size()) {
	       logg( ERROR, "neighbors.size " + to_string( V[i].neighbors.size() )
		     + " v_neighbors.size " + to_string( V[i].v_neighbors.size() ));

	       return false;
	    }
	    while (it1 != V[i].neighbors.end()) {
	       if (*it1 != *it3)
		  return false;
	       if ( (*it1)->other( i ) != *it2 )
		  return false;

	       ++it1; ++it2; ++it3;
	    }
	 }

	 return true;
      }

      void genER( unsigned erN, double erP ) {
	 logg(INFO, "Generating ER graph, n = " + to_string(erN) + ", p = " + to_string(erP));
	 this->n = erN;
	 init_empty_graph();
	 uniform_real_distribution< double > urd1 (0.0, 1.0);
	 for (unsigned i = 0; i < this->n; ++i) {
	    for (unsigned j = i + 1 ; j < this->n; ++j) {
	       if (urd1( gen ) <= erP) {
		  add_edge( i, j );
	       }
	    }
	 }

	 logg(INFO, "Graph constructed: n = " + to_string(n) + ", m = " + to_string(m));
	 logg(INFO, "Sorting neighbor ids..." );
	 clock_t t_start = clock();
	 for ( auto v = V.begin(); v != V.end(); ++v ) {
	    auto p = sort_permutation( v->v_nei_ids, mycompare );
	    apply_permutation_in_place( v->v_nei_ids, p );
	    apply_permutation_in_place( v->v_neighbors, p );
	 }
	 preprocessTime = double (clock() - t_start) / CLOCKS_PER_SEC;
      }
    
      void read_edge_list_bin( string fname ) {
	 ifstream ifile ( fname.c_str(), ios::in | ios::binary );
	 unsigned m; //number of edges in list
	 unsigned n;
	 ifile.read( (char*) &n, sizeof( unsigned ) );
	 ifile.read( (char*) &m, sizeof( unsigned ) );
	 unsigned* fromto_arr = new unsigned [2 * m];
	 
	 ifile.read( (char *) fromto_arr,  2 * m *sizeof( unsigned ) );

	 logg(INFO, "File input finished. Constructing graph..." );
	 this->n = n;
	 init_empty_graph();
	 for (unsigned i = 0; i < m; ++i) {
	    unsigned from = fromto_arr[ 2 * i ];
	    unsigned to = fromto_arr[ 2 * i + 1 ];

	    add_edge( from, to );
	 }

	 delete [] fromto_arr;

	 // unsigned from,to;
	 // for (unsigned i = 0; i < m; ++i) {
	 //    ifile.read( (char *) &from,  sizeof( unsigned ) );
	 //    ifile.read( (char *) &to,  sizeof( unsigned ) );

	 //    add_edge( from, to );
	 // }
	 // ifile.close();

	 
	 logg(INFO, "Graph constructed: n = " + to_string(n) + ", m = " + to_string(m));

	 logg(INFO, "Sorting neighbor ids..." );
	 clock_t t_start = clock();
	 for ( auto v = V.begin(); v != V.end(); ++v ) {
	    auto p = sort_permutation( v->v_nei_ids, mycompare );
	    //				       [](const node_id& a, const node_id& b){ return a < b; } );
	    apply_permutation_in_place( v->v_nei_ids, p );
	    //	    sort( v->v_nei_ids.begin(), v->v_nei_ids.end() );
	    apply_permutation_in_place( v->v_neighbors, p );
	    //	    v->neighbors.assign( v->v_neighbors.begin(),
	    //				 v->v_neighbors.end() );
	 }
	 preprocessTime = double (clock() - t_start) / CLOCKS_PER_SEC;
      }
      
      void read_edge_list( string fname ) {
	 ifstream ifile ( fname.c_str() ); 
	 string line;
	 unsigned linenum = 0;
	 istringstream iss;
	 node_id from, to; //for each edge

	 logg(INFO, "Reading graph from edge list: " + fname);
      
	 while ( getline( ifile, line ) ) {
	    if ( line[0]  != '#' && line[0] != ' ') {
	       if (line.size() == 0)
		  break;
	  
	       iss.clear();
	       iss.str( line );
	       //need to add an edge 
	       iss >> from;
	       iss >> to;
	  
	       add_edge( from, to );
	       //	       print_vector( V[from].v_nei_ids, cerr );
	       //	       print_vector( V[to].v_nei_ids, cerr );
	       ++linenum;
	    }
	 }

	 //Graph has been constructed
	 ifile.close();

	 logg(INFO, "Graph constructed: n = " + to_string(n) + ", m = " + to_string(m));

	 logg(INFO, "Sorting neighbor ids..." );
	 clock_t t_start = clock();
	 for ( auto v = V.begin(); v != V.end(); ++v ) {
	    auto p = sort_permutation( v->v_nei_ids, mycompare );
	    //				       [](const node_id& a, const node_id& b){ return a < b; } );
	    apply_permutation_in_place( v->v_nei_ids, p );
	    //	    sort( v->v_nei_ids.begin(), v->v_nei_ids.end() );
	    apply_permutation_in_place( v->v_neighbors, p );
	    //	    v->neighbors.assign( v->v_neighbors.begin(),
	    //				 v->v_neighbors.end() );
	 }
	 preprocessTime = double (clock() - t_start) / CLOCKS_PER_SEC;
	 // logg(INFO, "Copying edges..." );
	 // vE.assign(E.begin(),E.end());
	 // for (size_t i = 0; i < V.size(); ++i) {
	 //    logg(DEBUG, to_string(i) + " neighbors: ");
	 //    for (unsigned j= 0; j < V[i].v_nei_ids.size(); ++j) {
	 //       logg(DEBUG, to_string( V[i].v_nei_ids[j] ) );
	 //    }
	 // }
      }

   };

   //Implementation of Segment class
   
   
   Segment::Segment( node_id s, size_t l ) {
      length = l;
      V.assign( length + 1, s ); //length + 1 because we are storing vertices, not edges
      Index.assign( length + 1, -1 ); //No index info. needed for s
      //V.push_back( s );
      //Index.push_back( 0 ); //No information needed about s
   }

   bool Segment::exhFillOutSegment( size_t pos, Graph& G ) {
      //Find first valid segment of length "length"
      //With initial segment up to pos already defined
      size_t startPos = pos;
      ++pos;
      while ( pos <= length ) {
	 if (pos == startPos) {
	    return false;
	 } 
	 
	 node_id& prior = V[ pos - 1 ];
	 vector< node_id >& vNeis = G.V[prior].v_nei_ids;
	 ++Index[ pos ];
	 if ( static_cast< size_t > ( Index[ pos ] ) >= vNeis.size()) {
	    Index[pos ] = -1;
	    --pos;
	    continue;
	 }
	 
	 V[ pos ] = vNeis[ Index[ pos ] ];
	 
	 ++pos;
      }

      //Check if segment contains repeated vertices
      for (size_t i = 0; i < V.size(); ++i) {
	 if ( chosen[ V[ i ] ] ) {
	    for (size_t j = 0; j < i; ++j) {
	       chosen[ V[ j ] ] = false;
	    }
	    
	    return false;
	 } else {
	    chosen[ V[ i ] ] = true;
	 }
      }

      for (size_t j = 0; j < V.size(); ++j) {
	 chosen[ V[ j ] ] = false;
      }
      
      return true;
   }
   
   bool Segment::fillOutSegment( size_t pos, Graph& G ) {
      //Find first valid segment of length "length"
      //With initial segment up to pos already defined
      size_t startPos = pos;
      ++pos;
      while ( pos <= length ) {
	 if (pos == startPos)
	    return false;
	 
	 node_id& prior = V[ pos - 1 ];
	 vector< node_id >& vNeis = G.V[prior].v_nei_ids;
	 int& neiIndex = Index[ pos ];

	 ++neiIndex;
	 if ( static_cast< size_t > ( neiIndex ) >= vNeis.size()) {
	    //backtrack
	    Index[ pos ] = -1;
	    --pos;
	    continue;
	 }

	 while ( vNeis[ neiIndex ] < prior ) {
	    ++neiIndex;

	    if (static_cast< size_t >(neiIndex) >= vNeis.size()) {
	       Index[ pos ] = -1;
	       --pos;
	       break;
	    }
	 }

	 if ( static_cast< size_t >( neiIndex ) < vNeis.size()) {
	    V[ pos ] = vNeis[ neiIndex ];
	 
	    ++pos;
	 }
      }
      
      return true;
   }
      
   bool Segment::nextSegment( Graph& G ) {

      if (V[1] == V[0]) {
	 //We are unitialized.
	 //Attempt initialization with valid segment
	 return fillOutSegment( 0, G );
      }
	 
      size_t pos = length;

      do {
	 ++Index[ pos ];
	 if ( static_cast<size_t>(Index[pos]) > (G.V[ V[ pos - 1 ] ].v_nei_ids.size() - 1) ) {
	    Index[ pos ] = -1;
	    --pos;
	    
	    if (pos == 0)
	       return false;

	    continue;
	 }

	 //Found a position whence we can search for next segment
	 V[pos] = G.V[ V[ pos - 1 ] ].v_nei_ids[ Index[ pos ] ];
	 if ( fillOutSegment( pos, G ) ) {
	    return true;
	 }
	 
      } while ( true );

      
      
      return false;
   }

   bool Segment::exhaustiveNextSegment( Graph& G ) {

      size_t pos = length;

      if (pos == 1)
	 return false;
      
      do {
	 ++Index[ pos ];
	 if ( static_cast< size_t >( Index[pos] ) > (G.V[ V[ pos - 1 ] ].v_nei_ids.size() - 1) ) {
	    --pos;
	    
	    if (pos == 1)
	       return false;

	    continue;
	 }

	 //Found a position whence we can search for next segment
	 V[pos] = G.V[ V[ pos - 1 ] ].v_nei_ids[ Index[ pos ] ];
	 if ( exhFillOutSegment( pos, G ) ) {
	    return true;
	 } 
	 
      } while ( true );

      
      
      return false;
   }

   node_id Segment::last() {
      return V.back();
   }
   


}


#endif
