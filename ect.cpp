#include <iostream>
#include "ArbCycleGraph.cpp"
//#include "glpk_solver.cpp"
//#include "arbCycleLPSolver.cpp"
#include "arbCycleCPLEXSolver.cpp"
#include <string>
#include <iomanip>
#include <ctime>
#include <unistd.h>
#include <random>
#include "proc_size.cpp"
#include <chrono>

using namespace arbCycleGraph;

enum Algo {DARC, CARL, KORT, OPT, PREPROCESS, NONE, PD };

void outputFile( ostream& os,
		 string& fname,
		 string algName,
		 unsigned n,
		 unsigned m,
		 double preprocessTime,
		 unsigned solSize,
		 double runningTime ) {
  os << fname;
  os << ' ';
  os << algName << ' ';
  os << n << ' ';
  os << m << ' ';
  os << preprocessTime << ' ';
  os << solSize << ' ';
  os << runningTime << ' ';
  os << getPeakRSS() / (1024.0 * 1024.0) << endl; //peak resources used in Mb
}
		 

void print_help() {
  cout << "Options: " << endl;
  cout << "-G <graph filename in edge list format>" << endl
       << "-g <graph filename in binary edge list format>" << endl
       << "-k <cycle length>" << endl
       << "-D [run DARC]" << endl
       << "-K [run KORTSARZ]" << endl
       << "-C [run CARL]" << endl
       << "-O [run OPT]" << endl
       << "-P [run PRIMALDUAL]" << endl
       << "-t [time limit in hours, default is 4]" << endl
       << "-x [maximum number of threads]]" << endl;
   
}

using namespace std;

int main(int argc, char ** argv) {
  int c;
  extern char *optarg;
  //  extern int optint, optopt;

  if (argc == 1) {
     print_help();
     return 1;
  }

  string fname;
  
  string s_arg;
  string outfilename;
  unsigned erN = 0;
  double erP;
  bool bBinaryFormat = false;
  size_t k = 0;
  Algo alg = NONE;
  size_t nThreads = 1;
  string outFileName;
  bool bVerb = false;
  
  while ((c = getopt( argc, argv, ":G:g:k:pPDECOKx:o:v") ) != -1) {
    switch(c) {
    case 'v':
       bVerb = true;
       break;
    case 'o':
       outFileName.assign( optarg );
       break;
    case 'x':
       s_arg.assign( optarg );
       nThreads = stoi( s_arg );
       break;
    case 'K':
       alg = KORT;
       break;
    case 'O':
       alg = OPT;
       break;
    case 'P':
       alg = PD;
       break;
    case 'p':
       alg = PREPROCESS;
       break;
    case 'D':
       alg = DARC;
       break;
    case 'C':
       alg = CARL;
       break;
    case 'E':
       break;
    case 'k':
       s_arg.assign( optarg );
       k = stoi( s_arg );
       break;
    case 'G':
      //graph specification
      fname.assign( optarg );
      
      break;
    case 'g':
       fname.assign( optarg );
       bBinaryFormat = true;
       break;
    case '?':
      print_help();
      return 1;
      break;
    }
  }
  
  if (fname.size() == 0) {
     print_help();
     cerr << "Input graph is required.\n";
     return 1;
  } else {
    if (fname.substr(0, 2) == "ER") {
      size_t pos_colon = fname.find_first_of( ':' );
      string s_n = fname.substr(2, (pos_colon - 2));
      string s_p = fname.substr( pos_colon + 1 );
      erP = stod( s_p );
      erN = stoi( s_n );
    }
  }

  Graph G(INFO, cout);

  if (k <= 2) {
     G.logg << ERROR << "k must be at least 3." << endL;
     exit( 1 );
  }
  
  G.logg(INFO, "Reading graph...");
  if (bBinaryFormat) {
     G.read_edge_list_bin( fname );
  } else {
     if (erN > 0) {
	//generate ER graph
	G.genER( erN, erP );
     } else {
	G.read_edge_list( fname );
     }
  }
	
  G.logg(INFO, "Basic graph info (n, m): " + to_string( G.V.size() ) + " "  + to_string( G.E.size() ) );

  size_t sizeS;
  double runningTime;
  clock_t t_start = clock();
  switch (alg) {
  case PREPROCESS:
     G.list_cycles( k );
     break;
  case PD:
     G.primal_dual( k );
     break;
  case DARC:
     G.darc_base( k );
     break;
  // case DARCFREE:
  //    G.darc_base_free( k );
  //    sizeS = G.countS();
  //    G.logg << "Size of solution: " << sizeS << endL;
  //    break;
  case CARL:
     //glpk_carl( G, k );
     carl(G, k, nThreads, bVerb );

     break;
  case KORT:

     //glpk_kortsarz( G, k );
     kortsarz( G, k, nThreads, bVerb );

     break;
  case OPT:
     {
	vector< double > vsol;
	G.list_cycles(k , true );
	//GLPK_solver GLPK( G, k );

	//if (GLPK.MIP_solve( G )) {
	//sizeS = G.countS();
	//G.logg << "Size of solution: " << sizeS << endL;
	//	}

	solve_ip( G, vsol, nThreads, bVerb );
     }
     
     break;
  case NONE:
     break;
  }
  runningTime = double( clock() - t_start ) / CLOCKS_PER_SEC;
  
  sizeS = G.countS();
  G.logg << "Size of solution: " << sizeS << endL;
  G.logg << "Time elapsed: " << runningTime << endL;
  //if (G.bruteTest( k )) {
  //     cout << "Solution is feasible." << endl;
  //  }

  if (outFileName.size() > 0) {
     //Output results to a file
     ofstream ofile;
     ofile.open( outFileName.c_str(), ios::app );
     ofile << k << '\t' << alg << '\t' << sizeS << '\t' << runningTime << endl;
     ofile.close();
  }
  
  return 0;
}

